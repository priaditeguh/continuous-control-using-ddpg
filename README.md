# Continuous Control #

For this project, we work with the [Reacher](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Examples.md#reacher) environment. In this environment, a double-jointed arm can move to target locations. A reward of +0.1 is provided for each step that the agent's hand is in the goal location. Thus, the goal of the agent is to maintain its position at the target location for as many time steps as possible.

The observation space consists of 33 variables corresponding to position, rotation, velocity, and angular velocities of the arm. Each action is a vector with four numbers, corresponding to torque applicable to two joints. Every entry in the action vector should be a number between -1 and 1.

In this project, we have 20 identical agents, each with its own copy of the environment. It has been shown that having multiple copies of the same agent [sharing experience can accelerate learning](https://ai.googleblog.com/2016/10/how-robots-can-acquire-new-skills-from.html). We use [DDPG](https://openreview.net/pdf?id=SyZipzbCb) algorithm that use multiple (non-interacting, parallel) copies of the same agent to distribute the task of gathering experience.

![20 Arm Robots](image/reacher.gif)

The goal of this project is that the agents must get an average score of +30 (over 100 consecutive episodes, and over all agents). In other words, after each episode, we add up the rewards that each agent received (without discounting), to get a score for each agent. This yields 20 (potentially different) scores. We then take the average of these 20 scores. This yields an average score for each episode (where the average is over all 20 agents).

## Library ##
1. python 3.6.8
2. gym 0.17.2
3. torch 1.6.0
4. matplotlib 3.3.1
5. numpy 1.19.1
7. unityagents 0.4.0

For more details, you can check `requirements.txt`.

### File Structure ###
```
- model.py # the neural network model as function approximator
- ddpg_agent.py # Deep Deterministic Policy Gradient algorithm code for 1 agent
- ddpg_20agent.py # Deep Deterministic Policy Gradient algorithm code for 20 agent
- Continuous Control using DDPG - 1 Agents.ipynb # notebook to train 1 agent
- Continuous Control using DDPG - 20 Agents.ipynb # notebook to train 20 agent
- checkpoint_actor.pth # weights of trained actor neural network of 20 agents
- checkpoint_critic.pth # weights of trained critic neural network of 20 agents
- checkpoint_actor_single.pth # weights of trained actor neural network of 1 agents
- checkpoint_critic_single.pth # weights of trained critic neural network of 1 agents
- README.md
- requirements.txt
```

### Instruction ###
For this project, we will not need to install Unity - this is because we can download the ready-to-use environment from one of the links below. You need only select the environment that matches your operating system:

- Linux: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P2/Reacher/Reacher_Linux.zip)
- Mac OSX: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P2/Reacher/Reacher.app.zip)
- Windows (32-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P2/Reacher/Reacher_Windows_x86.zip)
- Windows (64-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Reacher_Windows_x86_64.zip)

Then, place the files in this repository.

After that, you can open `Continuous Control using DDPG - 1 Agents.ipynb` to train a single agent. Or you can use `Continuous Control using DDPG - 20 Agents` to train 20 agents.

If you would like to change the neural network model, you can change `model.py`. And if you want to improve the algorithm, you can modify `ddpg_agent.py` or `ddpg_20agent.py`.

### Results  ###

The reward plot of 1 Agent.

![Single Agent](image/1agents_plot.PNG) 

The average reward plot of 20 Agents.

![20 Agents](image/20agents_plot.PNG) 

### Acknowledgement ###
1. [Udacity](https://www.udacity.com/)
2. [Unity](https://unity.com/)